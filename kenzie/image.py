import os
from environs import Env
from os import environ
from flask import jsonify, request, send_from_directory
import pdb


env = Env()
env.read_env()


max_size = environ.get('MAX_CONTENT_LENGTH')
extensions = environ.get("ALLOWED_EXTENSIONS")
extensions_list = extensions.split(':')
directory_list= environ.get("FILES_DIRECTORY")
dir_list = directory_list.split(':')
path = environ.get("ARCHIVES_DIRECTORY")


def make_image_repos():
    try:
        for dir in dir_list:
            os.mkdir(dir)
    except FileExistsError as err:
        return err


def list_function():
    dict_list = dict()
    for _, dirs, files in os.walk(path):
        for dir in dirs:
            dict_list[dir] = files

        for filename in files:
            for item in dict_list:
                if filename[-3::] == item:
                    dict_list[item] = [*dict_list[item], filename]

    return jsonify(dict_list)


def upload_function():
    archives = request.files

    for item in archives:
        archive = archives[item]
        archive_type = archive.filename[-3::]
        if archive_type in extensions_list:
            for _,_,files in os.walk(path):
                if archive.filename in files:
                    return {"message": "O arquivo já existe"}, 409
            archive.save(f"{path}/{archive_type}/{archive.filename}")
        else:
            return {"message": "O tipo do arquivo não é aceito"}, 415
    return {"message": "Upload realizado com sucesso!"}, 201


def list_specif_file_function(type: str):
    dict_list = dict()
    for _, dirs, files in os.walk(path):
        for dir in dirs:
            if dir == type:
                dict_list[dir] = files

        for filename in files:
            for item in dict_list:
                if filename[-3::] == item:
                    dict_list[item] = [*dict_list[item], filename]

    return jsonify(dict_list)


def download_specifc_file(file: str):
    extension = file[-3::]
    for _,_,files in os.walk(f'{path}/{extension}'):
        if file in files:
            return send_from_directory(directory=f".{path}/{extension}", path = file,as_attachment=True), 201

        return {"message": "Arquivo não encontrado"}, 404

def download_dir_as_zip_function(extension_query: str, ratio_query: str):
    for _,_,files in os.walk(f'{path}/{extension_query}'):
        if len(files) > 0:    
            extension_for_zip = extension_query
            compression_ratio = ratio_query

            comand = f'zip -{compression_ratio} -r /tmp/compressed_{extension_for_zip}.zip {path}/{extension_for_zip}'
            os.system(comand)

            return send_from_directory(directory=f"/tmp/", path=f"compressed_{extension_for_zip}.zip", as_attachment=True), 201
        else:
            return {"message": "Nenhum arquivo disponível"}, 404
