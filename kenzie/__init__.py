import pdb
from flask import Flask, request
from environs import Env
from os import environ
from .image import list_function, upload_function, list_specif_file_function, download_specifc_file, download_dir_as_zip_function, make_image_repos


env = Env()

env.read_env()
app = Flask(__name__)
max_size = environ.get('MAX_CONTENT_LENGTH')
app.config['MAX_CONTENT_LENGTH'] = int(max_size)

make_image_repos()

@app.errorhandler(413)
def too_large_error(e):
    return {"message": "O tamanho do arquivo é maior do que o máximo aceito"}, 413

    

@app.post("/upload")
def upload_archive():
    return upload_function()


@app.get("/files")
def list_files():
    return list_function()


@app.get("/files/<string:extension>")
def list_files_by_extension(extension: str):
    return list_specif_file_function(extension)


@app.get("/download/<string:file_name>")
def download(file_name: str):
    return download_specifc_file(file_name)

@app.get("/download-zip")
def download_dir_as_zip():
    extension = request.args.get("file_extension")
    compression = request.args.get("compression_ratio")
    
    return download_dir_as_zip_function(extension, compression)
